import random
import sqlite3

from src.config.config import DB_PATH_OBJ


def map_one_entity_id_to_another(table, num=1):
    con = sqlite3.connect(DB_PATH_OBJ)
    cur = con.cursor()

    output = []
    for i in range(num):
        motorcycles_count = int(cur.execute('SELECT COUNT(*) FROM {}'.format(table)).fetchall()[0][0])
        output.append(random.randint(1, motorcycles_count+1))

    return output


if __name__ == "__main__":
    print(map_one_entity_id_to_another('motorcycle', num=10))
