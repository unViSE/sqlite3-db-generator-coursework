from pathlib import Path

WORKDIR_PATH_OBJ = Path(__file__).resolve().parent.parent.parent
SRC_PATH_OBJ = Path(__file__).resolve().parent.parent
DB_PATH_OBJ = Path(WORKDIR_PATH_OBJ, 'db.sqlite3').resolve()


if __name__ == "__main__":
    print(WORKDIR_PATH_OBJ)
    print(SRC_PATH_OBJ)
    print(DB_PATH_OBJ)
