import sys

import sqlite3

from src.config.config import DB_PATH_OBJ
from src.entity_generation.motorcycle import generate_random_motorcycle
from src.entity_generation.geolocation import generate_geolocation
from src.entity_generation.user_accident_place_list import generate_user_accident_place_list
from src.entity_generation.user_emergency_service_list import generate_user_emergency_service_list
from src.entity_generation.velocity import generate_velocity
from src.entity_generation.emergency_service import generate_emergency_service
from src.entity_generation.user import generate_user
from src.entity_generation.accident_place import generate_accident_place
from src.entity_generation.const import entity_attrs as e_attrs
from src.db_generation_data.generate import generate
from src.db_generation_data.const import queries


def main():
    con, cur = None, None

    try:
        con = sqlite3.connect(DB_PATH_OBJ)
        cur = con.cursor()

        size = 100

        generate(con, cur, queries['motorcycle']['insert'], e_attrs['motorcycle'], generate_random_motorcycle(size))

        generate(con, cur, queries['geolocation']['insert'], e_attrs['geolocation'], generate_geolocation(size))

        generate(con, cur, queries['emergency_service']['insert'], e_attrs['emergency_service'],
                 generate_emergency_service(size))

        generate(con, cur, queries['velocity']['insert'], e_attrs['velocity'], generate_velocity(size))

        generate(con, cur, queries['user']['insert'], e_attrs['user'], generate_user(size))

        generate(con, cur, queries['accident_place']['insert'], e_attrs['accident_place'],
                 generate_accident_place(size))

        generate(con, cur, queries['user_accident_place_list']['insert'], e_attrs['user_accident_place_list'],
                 generate_user_accident_place_list(size))

        generate(con, cur, queries['user_emergency_service_list']['insert'], e_attrs['user_emergency_service_list'],
                 generate_user_emergency_service_list())

    except sqlite3.Error as e:
        print(f"Error {e.args[0]}")
        sys.exit(1)

    finally:
        if con:
            con.close()


if __name__ == "__main__":
    main()
