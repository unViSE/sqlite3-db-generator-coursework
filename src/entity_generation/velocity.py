import numpy as np


def generate_velocity(num=1):
    output = []
    for x in range(num):
        output.append({
            'velocity': np.random.randint(low=30, high=150)
        })
    return output


if __name__ == '__main__':
    print(generate_velocity())
