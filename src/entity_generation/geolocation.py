from faker import Faker


def generate_geolocation(num=1):
    fake = Faker()
    
    output = []
    for x in range(num):
        output.append({
            'latitude': float(fake.latitude()),
            'longitude': float(fake.longitude())
        })
    return output


if __name__ == '__main__':
    print(generate_geolocation())
