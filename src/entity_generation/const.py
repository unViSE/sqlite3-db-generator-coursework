entity_attrs = {
    'user': ['first_name', 'last_name', 'patronymic', 'email', 'phone', 'date_created', 'date_updated', 'status',
             'FK_motorcycle'],
    'motorcycle': ['brand', 'model', 'license_plate'],
    'velocity': ['velocity'],
    'emergency_service': ['phone'],
    'geolocation': ['latitude', 'longitude'],
    'accident_place': ['FK_accident_place_geolocation', 'FK_accident_place_velocity'],
    'user_accident_place_list': ['FK_user', 'FK_accident_place'],
    'user_emergency_service_list': ['FK_user', 'FK_emergency_service']
}
