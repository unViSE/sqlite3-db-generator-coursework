import numpy as np
from faker import Faker
from faker_vehicle import VehicleProvider


def generate_random_motorcycle(num=1, seed=None):
    fake = Faker('ru_RU')
    fake.add_provider(VehicleProvider)
    np.random.seed(seed)

    output = []
    for x in range(num):
        output.append({
            'brand': fake.vehicle_make(),
            'model': fake.vehicle_model(),
            'license_plate': fake.license_plate()
        })

    return output


if __name__ == '__main__':
    print(generate_random_motorcycle())
