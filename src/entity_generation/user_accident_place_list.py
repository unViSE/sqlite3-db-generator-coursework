from src.random_relationship.map import map_one_entity_id_to_another


def generate_user_accident_place_list(num=1):
    output = []
    for x in range(num):
        output.append({
            'FK_user': map_one_entity_id_to_another('user')[0],
            'FK_accident_place': map_one_entity_id_to_another('accident_place')[0]
        })
    return output


if __name__ == '__main__':
    print(generate_user_accident_place_list())
