import random
import sqlite3

from src.config.config import DB_PATH_OBJ
from src.random_relationship.map import map_one_entity_id_to_another


def generate_user_emergency_service_list():
    con = sqlite3.connect(DB_PATH_OBJ)
    cur = con.cursor()

    output = []
    for _ in range(1, int(cur.execute("SELECT COUNT(*) FROM user").fetchall()[0][0])+1):
        for _ in range(1, random.randint(2, 5)):
            output.append({
                'FK_user': map_one_entity_id_to_another('user')[0],
                'FK_emergency_service': map_one_entity_id_to_another('emergency_service')[0]
            })
    return output


if __name__ == '__main__':
    print(generate_user_emergency_service_list())
