from faker import Faker


def generate_emergency_service(num=1):
    fake = Faker()

    output = []
    for x in range(num):
        output.append({
            'phone': fake.phone_number()
        })
    return output


if __name__ == '__main__':
    print(generate_emergency_service())
