from datetime import datetime

import numpy as np
from faker import Faker

from src.random_relationship.map import map_one_entity_id_to_another


def generate_user(num=1, seed=None):
    fake = Faker('ru_RU')
    np.random.seed(seed)

    output = []
    for x in range(num):
        gender = np.random.choice(["M", "F"], p=[0.5, 0.5])
        date_created = fake.date_between_dates(date_start=datetime(2001, 1, 1), date_end=datetime.now())
        date_updated = fake.date_between_dates(date_start=date_created, date_end=datetime.now())

        output.append({
            'first_name': fake.first_name_male() if gender == 'M' else fake.first_name_female(),
            'last_name': fake.last_name_male() if gender == 'M' else fake.last_name_female(),
            'patronymic': fake.middle_name_male() if gender == 'M' else fake.middle_name_female(),
            'email': fake.email(),
            'phone': fake.phone_number(),
            'date_created': date_created.strftime('%Y-%M-%d'),
            'date_updated': date_updated.strftime('%Y-%M-%d'),
            'status': int(np.random.choice([0, 1], p=[0.5, 0.5])),
            'FK_motorcycle': map_one_entity_id_to_another('motorcycle')[0]
        })
    return output


if __name__ == '__main__':
    print(generate_user())
