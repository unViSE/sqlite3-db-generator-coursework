from src.random_relationship.map import map_one_entity_id_to_another


def generate_accident_place(num=1):
    output = []
    for x in range(num):
        output.append({
            'FK_accident_place_geolocation': map_one_entity_id_to_another('geolocation')[0],
            'FK_accident_place_velocity': map_one_entity_id_to_another('velocity')[0]
        })
    return output


if __name__ == '__main__':
    print(generate_accident_place())
