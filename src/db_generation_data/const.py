queries = {
    'motorcycle': {
        'insert': "INSERT INTO motorcycle(brand, model, license_plate) VALUES(?, ?, ?)"
    },
    'geolocation': {
        'insert': "INSERT INTO geolocation(latitude, longitude) VALUES(?, ?)",
    },
    'velocity': {
        'insert': "INSERT INTO velocity(velocity) VALUES(?)",
    },
    'emergency_service': {
        'insert': "INSERT INTO emergency_service(phone) VALUES(?)"
    },
    'user': {
        'insert': """
        INSERT INTO user(first_name, last_name, patronymic, email, phone, date_created, date_updated, status,
            FK_motorcycle)
        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)
        """
    },
    'accident_place': {
        'insert': "INSERT INTO accident_place(FK_accident_place_geolocation, FK_accident_place_velocity) VALUES (?, ?)"
    },
    'user_accident_place_list': {
        'insert':
            """
            INSERT INTO user_accident_place_list(FK_user, FK_accident_place) 
            VALUES (?, ?)
            """
    },
    'user_emergency_service_list': {
        'insert':
            """
            INSERT INTO user_emergency_service_list(FK_user, FK_emergency_service) 
            VALUES (?, ?)
            """
    }
}
