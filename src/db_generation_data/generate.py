from sqlite3 import Connection, Cursor, Error


def generate(con: Connection, cur: Cursor, query: str, params: list, fun):
    try:
        for i, dictionary in enumerate(fun):
            values = [dictionary[i] for i in params]

            cur.execute(query, values)
            con.commit()

    except Error as e:
        print(f"Error {e.args[0]}")
        con.rollback()
