DROP TABLE IF EXISTS 'accident_place'
;

DROP TABLE IF EXISTS 'emergency_service'
;

DROP TABLE IF EXISTS 'velocity'
;

DROP TABLE IF EXISTS 'geolocation'
;

DROP TABLE IF EXISTS 'motorcycle'
;

DROP TABLE IF EXISTS 'user'
;

DROP TABLE IF EXISTS 'user_emergency_service_list'
;

DROP TABLE IF EXISTS 'user_accident_place_list'
;

/* Create Tables with Primary and Foreign Keys, Check and Unique Constraints */

CREATE TABLE 'accident_place'
(
	'id' INTEGER PRIMARY KEY,
	'FK_accident_place_geolocation' INTEGER NOT NULL,
	'FK_accident_place_velocity' INTEGER NOT NULL,
	FOREIGN KEY('FK_accident_place_geolocation') REFERENCES 'geolocation' ('id') ON DELETE No Action ON UPDATE No Action,
	FOREIGN KEY('FK_accident_place_velocity') REFERENCES 'velocity' ('id') ON DELETE No Action ON UPDATE No Action
)
;

CREATE TABLE 'emergency_service'
(
	'id' INTEGER PRIMARY KEY,
	'phone' TEXT NOT NULL
)
;

CREATE TABLE 'velocity'
(
	'id' INTEGER PRIMARY KEY,
	'velocity' REAL NOT NULL
)
;

CREATE TABLE 'geolocation'
(
	'id' INTEGER PRIMARY KEY,
	'latitude' REAL NOT NULL,
	'longitude' REAL NOT NULL
)
;

CREATE TABLE 'motorcycle'
(
	'id' INTEGER PRIMARY KEY,
	'brand' TEXT NOT NULL,
	'model' TEXT NOT NULL,
	'license_plate' TEXT NOT NULL
)
;

CREATE TABLE 'user'
(
	'id' INTEGER PRIMARY KEY,
	'first_name' TEXT NOT NULL,
	'last_name' TEXT NOT NULL,
	'patronymic' TEXT NOT NULL,
	'email' TEXT NOT NULL,
	'phone' TEXT NOT NULL,
	'date_created' TEXT NOT NULL,
	'date_updated' TEXT NOT NULL,
	'status' INTEGER NOT NULL,
	'FK_motorcycle' INTEGER NOT NULL,
	FOREIGN KEY('FK_motorcycle') REFERENCES 'motorcycle' ('id') ON DELETE No Action ON UPDATE No Action
)
;

CREATE TABLE 'user_emergency_service_list'
(
    'id' INTEGER PRIMARY KEY,
    'FK_user' INTEGER NOT NULL,
    'FK_emergency_service' INTEGER NOT NULL,
    FOREIGN KEY('FK_user') REFERENCES 'user' ('id') ON DELETE No Action ON UPDATE No Action,
    FOREIGN KEY('FK_emergency_service') REFERENCES 'emergency_service' ('id') ON DELETE No Action ON UPDATE No Action
)
;

CREATE TABLE 'user_accident_place_list'
(
    'id' INTEGER PRIMARY KEY,
    'FK_user' INTEGER NOT NULL,
    'FK_accident_place' INTEGER NOT NULL,
    FOREIGN KEY('FK_user') REFERENCES 'user' ('id') ON DELETE No Action ON UPDATE No Action,
    FOREIGN KEY('FK_accident_place') REFERENCES 'accident_place' ('id') ON DELETE No Action ON UPDATE No Action
)
;

/* Create Indexes and Triggers */

CREATE INDEX 'IXFK_accident_place_geolocation'
 ON 'accident_place' ('id' ASC)
;

CREATE INDEX 'IXFK_accident_place_velocity_characteristics'
 ON 'accident_place' ('id' ASC)
;

CREATE INDEX 'IXFK_user_accident_place'
 ON 'user' ('id' ASC)
;

CREATE INDEX 'IXFK_user_emergency_service'
 ON 'user' ('id' ASC)
;

CREATE INDEX 'IXFK_user_motorcycle'
 ON 'user' ('id' ASC)
;

/* todo: create indices for tables user_emergency_service_list and user_accident_place_list */
